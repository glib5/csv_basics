# https://docs.python.org/3/library/xml.etree.elementtree.html#module-xml.etree.ElementTree





import os 

def read_file():
    import xml.etree.ElementTree as ET

    tree = ET.parse("example.xml")
    # root = tree.getroot()
    return 


def make_file1():
    from xml.dom import minidom

    root = minidom.Document()
    
    xml = root.createElement('root') 
    root.appendChild(xml)
    
    productChild = root.createElement('product')
    productChild.setAttribute('name', 'Geeks for Geeks')
    productChild.setAttribute('name', 'Geeks for Geeks')
    xml.appendChild(productChild)

    productChild = root.createElement('product2')
    productChild.setAttribute('name2', 'Geeks for Geeks2')
    xml.appendChild(productChild)
    
    with open("created1.xml", "w") as f:
        # best format
        xml_str = root.toprettyxml(indent ="\t") 
        f.write(xml_str) 

    return 


def make_file2():
    import xml.etree.ElementTree as ET
      
    root = ET.Element("Catalog")
      
    m1 = ET.Element("mobile")
    root.append(m1)
      
    b1 = ET.SubElement(m1, "brand")
    b1.text = "Redmi"
    b2 = ET.SubElement(m1, "price")
    b2.text = "6999"
      
    m2 = ET.Element("mobile")
    root.append (m2)
      
    c1 = ET.SubElement(m2, "brand")
    c1.text = "Samsung"
    c2 = ET.SubElement(m2, "price")
    c2.text = "9999"
      
    m3 = ET.Element("mobile")
    root.append (m3)
      
    d1 = ET.SubElement(m3, "brand")
    d1.text = "RealMe"
    d2 = ET.SubElement(m3, "price")
    d2.text = "11999"

    with open("created2.xml", "wb") as files:
        tree = ET.ElementTree(root)
        tree.write(files)

    return 

def main():

    read_file()

    make_file1()

    # make_file2()
    
    return


if __name__=="__main__":
    main()

    # input()