"""
handles the possibility to dump and load datetimes in a .json file
- data has to be list of dict
"""

from datetime import date, datetime
from json import dump as jdump
from json import load as jload
from pathlib import Path
from typing import Dict, List


def _check(obj=None, file=None, read=False):
    if obj is not None:
        assert isinstance(obj, list)
        assert all(isinstance(d, dict) for d in obj)
    if file is not None:
        assert isinstance(file, Path)
        if read:
            assert file.exists()
        assert file.is_file()
        assert file.suffix == ".json"


def dump(jsonfile:Path, data:List[Dict]):
    _check(obj=data, file=jsonfile)
    _dump = [{k:(v.timetuple() if isinstance(v, (datetime, date)) else v) for k, v in d.items()} for d in data]
    with jsonfile.open("w") as fp:
        jdump(obj=_dump, fp=fp, indent=4)    


def load(jsonfile:Path) -> List[Dict]:
    _check(file=jsonfile, read=True)
    with jsonfile.open("r") as fp:
        file_content = jload(fp=fp)
    _check(obj=file_content)
    data = [{k:(datetime(*v[:6]) if isinstance(v, list) else v) for k, v in d.items()} for d in file_content]
    return data